﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Owin;

namespace NFinal.Middleware.UnZip4Test
{
    public class Startup
    {
        public void Configuration(IAppBuilder appBuilder)
        {
            string unZipFileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Static","Index.zip");
            FileStream fileStream = new FileStream(unZipFileName, FileMode.Open, FileAccess.Read);
            var config = new NFinal.Middleware.UnZip.UnZipConfig()
            {
                unZipArray = new[] { new NFinal.Middleware.UnZip.UnZip() {
                    zipFileStream=fileStream
                } },
                defaultUrl = "/Index.html"
            };
            appBuilder.Use<NFinal.Middleware.UnZip.OwinUnZipMiddleware>(config);
        }
    }
}
