﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NFinal.Middleware.UnZip4Test
{
    class Program
    {
        static void Main(string[] args)
        {
            string url = "http://localhost:8089";
            using (Microsoft.Owin.Hosting.WebApp.Start<NFinal.Middleware.UnZip4Test.Startup>(url))
            {
                Console.WriteLine("服务器已经启动");
                System.Diagnostics.Process.Start("explorer.exe", url);
                Console.ReadKey();
            }
        }
    }
}
