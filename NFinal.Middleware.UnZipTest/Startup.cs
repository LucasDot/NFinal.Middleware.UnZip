﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.FileProviders;
using System.IO;

namespace NFinal.Middleware.UnZipTest
{
    public class Startup
    {
        public void Configure(IApplicationBuilder app)
        {
            string unZipFileName = Path.Combine(AppContext.BaseDirectory, "Static", "Index.zip");
            FileStream fileStream = new FileStream(unZipFileName, FileMode.Open, FileAccess.Read);
            var config = new NFinal.Middleware.UnZip.UnZipConfig()
            {
                unZipArray = new[] { new NFinal.Middleware.UnZip.UnZip() {
                    zipFileStream=fileStream
                } },
                defaultUrl = "/Index.html"
            };
            app.UseMiddleware<NFinal.Middleware.UnZip.CoreUnZipMiddleware>(config);
        }
    }
}
