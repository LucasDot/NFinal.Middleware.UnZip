﻿using System;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using System.Threading.Tasks;

namespace NFinal.Middleware.UnZipTest
{
    class Program
    {
        static void Main(string[] args)
        {
            string url = "http://localhost:8089";
            var host = new WebHostBuilder()
                .UseKestrel()
                .UseUrls(url)
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseStartup<Startup>()
                .Build();
            Task.Run(() =>
            {
                host.Run();
            });
            Task.Run(() =>
            {
                System.Diagnostics.Process.Start("explorer.exe", url);
            });
            Console.ReadLine();
        }
    }
}