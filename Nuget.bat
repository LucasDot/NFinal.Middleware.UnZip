@echo off
call Build.bat
echo 开始复制.net core 1.1生成文件
copy /y NFinal.Middleware.UnZip\bin\Debug\netstandard1.3\NFinal.Middleware.UnZip.dll Nuget\lib\netcoreapp1.1
copy /y NFinal.Middleware.UnZip\bin\Debug\netstandard1.3\NFinal.Middleware.UnZip.xml Nuget\lib\netcoreapp1.1
echo ======================================================================
echo 开始复制.net standard 1.6生成文件
copy /y NFinal.Middleware.UnZip\bin\Debug\netstandard1.3\NFinal.Middleware.UnZip.dll Nuget\lib\netstandard1.3
copy /y NFinal.Middleware.UnZip\bin\Debug\netstandard1.3\NFinal.Middleware.UnZip.xml Nuget\lib\netstandard1.3
echo ======================================================================
echo 开始复制.net framework 4.0生成文件
copy /y NFinal.Middleware.UnZip4\bin\Debug\NFinal.Middleware.UnZip.dll Nuget\lib\net40-client
copy /y NFinal.Middleware.UnZip4\bin\Debug\NFinal.Middleware.UnZip.xml Nuget\lib\net40-client
echo ======================================================================
echo 开始复制.net framework 4.5生成文件
copy /y NFinal.Middleware.UnZip45\bin\Debug\NFinal.Middleware.UnZip.dll Nuget\lib\net45
copy /y NFinal.Middleware.UnZip45\bin\Debug\NFinal.Middleware.UnZip.xml Nuget\lib\net45
echo ======================================================================
echo 开始nuget打包
Nuget pack Nuget\NFinal.Middleware.UnZip.nuspec
pause
exit