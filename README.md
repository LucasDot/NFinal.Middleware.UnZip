# NFinal.Middleware.UnZip
把静态资源文件放入zip压缩包中的Http中间件项目,支持NFinal,Asp.net mvc以及.net core.

# .net framework下使用
```
public void Configuration(IAppBuilder appBuilder)
{
    string unZipFileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Static","Index.zip");
    FileStream fileStream = new FileStream(unZipFileName, FileMode.Open, FileAccess.Read);
    var config = new NFinal.Middleware.UnZip.UnZipConfig()
    {
        unZipArray = new[] { new NFinal.Middleware.UnZip.UnZip() {
            zipFileStream=fileStream
        } },
        defaultUrl = "/Index.html"
    };
    appBuilder.Use<NFinal.Middleware.UnZip.OwinUnZipMiddleware>(config);
}
```

# .net core及.net standard下使用
```
public void Configure(IApplicationBuilder app)
{
    string unZipFileName = Path.Combine(AppContext.BaseDirectory, "Static", "Index.zip");
    FileStream fileStream = new FileStream(unZipFileName, FileMode.Open, FileAccess.Read);
    var config = new NFinal.Middleware.UnZip.UnZipConfig()
    {
        unZipArray = new[] { new NFinal.Middleware.UnZip.UnZip() {
            zipFileStream=fileStream
        } },
        defaultUrl = "/Index.html"
    };
    app.UseMiddleware<NFinal.Middleware.UnZip.CoreUnZipMiddleware>(config);
}
```