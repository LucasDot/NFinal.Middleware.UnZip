﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
namespace NFinal.Middleware.UnZip
{
    public class UnZipConfig
    {
        public UnZip[] unZipArray;
        public string defaultUrl;
        public string accessControlAllowOrigin;
    }
    public class UnZip
    {
        public Stream zipFileStream;
        public string requestPrefix;
        public string password;
    }
}
