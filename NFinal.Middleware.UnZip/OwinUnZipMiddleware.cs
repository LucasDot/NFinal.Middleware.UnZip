﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace NFinal.Middleware.UnZip
{
    public class OwinUnZipMiddleware:UnZipMiddleware<IDictionary<string,object>>
    {
        public OwinUnZipMiddleware(InvokeDelegate<IDictionary<string, object>> next, UnZipConfig config) 
            :base(next,config)
        {

        }
        public override string GetRequestPath(IDictionary<string, object> context)
        {
            return (string)context["owin.RequestPath"];
        }
        public override void AddOrUpdateHeader(IDictionary<string, object> context, string key, string[] value)
        {
            IDictionary<string, string[]> headers = (IDictionary<string, string[]>)context["owin.ResponseHeaders"];
            if (!headers.ContainsKey(key))
            {
                headers.Add(key, value);
            }
            else
            {
                headers[key] =value;
            }
        }
        public override Stream GetResponseStream(IDictionary<string, object> context)
        {
            return (Stream)context["owin.ResponseBody"];
        }
        public override void SetStatusCode(IDictionary<string, object> context, int statusCode)
        {
            context["owin.ResponseStatusCode"] = statusCode;
        }
    }
}
