﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
#if NETCORE
using Microsoft.AspNetCore.Http;
#endif

namespace NFinal.Middleware.UnZip
{
#if NETCORE
    public class CoreUnZipMiddleware:UnZipMiddleware<HttpContext>
    {
        public CoreUnZipMiddleware(RequestDelegate next, UnZipConfig config)
            : base((context) => { return next.Invoke(context); }, config)
        {

        }
        public override string GetRequestPath(HttpContext context)
        {
            return context.Request.Path;
        }
        public override void AddOrUpdateHeader(HttpContext context, string key, string[] value)
        {
            if (context.Response.Headers.ContainsKey(key))
            {
                context.Response.Headers[key] = value;
            }
            else
            {
                context.Response.Headers.Add(key, value);
            }
        }
        public override Stream GetResponseStream(HttpContext context)
        {
            return context.Response.Body;
        }
        public override void SetStatusCode(HttpContext context, int statusCode)
        {
            context.Response.StatusCode = statusCode;
        }
    }
#endif
}
